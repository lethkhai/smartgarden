import { useEffect, useState } from "react";
import { useApi } from "../../hooks/UseApi";
import DeviceCard from "../../components/Card/DeviceCard";
import { useParams } from "react-router-dom";

type DeviceStruct = {
  device_id: string;
  device_name: string;
  status: string;
  time_on: string;
  time_off: string;
  recommend_time: number;
  garden_id: string;
};

type Datatype = {
  device: DeviceStruct[];
};

export default function DeviceData() {
  const { garden_id } = useParams();
  const [events, setEvents] = useState<DeviceStruct[]>([]);
  const [{ data, isLoading, isError }] = useApi<Datatype>(
    `http://localhost:8080/api/v1/garden/${garden_id}/device`
  );

  useEffect(() => {
    if (!isLoading && !isError && data && data.device !== undefined) {
      setEvents(() =>
        data.device.map((correspondingDevice) => ({
          device_id: correspondingDevice.device_id,
          device_name: correspondingDevice.device_name,
          status: correspondingDevice.status ? "ON" : "OFF",
          time_on: correspondingDevice.time_on,
          time_off: correspondingDevice.time_off,
          recommend_time: correspondingDevice.recommend_time,
          garden_id: correspondingDevice.garden_id,
        }))
      );
    }
  }, [data, isLoading, setEvents]);

  return (
    <div className="flex flex-wrap -mx-4 mt-[50px]">
      {events.map((event) => (
        <div
          key={event.device_id}
          className="w-full px-4 mb-8 sm:w-1/2 lg:w-1/2"
        >
          <DeviceCard event={event} />
        </div>
      ))}
    </div>
  );
}
