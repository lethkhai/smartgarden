import { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { FaRegCalendarAlt } from "react-icons/fa";
import { PiPlant } from "react-icons/pi";
import { ChartPieIcon } from "@heroicons/react/24/outline";

export default function Sidebar() {
  const { garden_id } = useParams();
  const navigate = useNavigate();
  const [open, setOpen] = useState(true);
  const Menus = [
    {
      title: "Dashboard",
      src: `${garden_id}`,
      icon: (
        <img src="/src/assets/dashboard-report-icon.png" className="w-5 h-4" />
      ),
    },
    {
      title: "Statistics",
      src: `${garden_id}/statistics`,
      icon: (
        // <img src="/src/assets/dashboard-report-icon.png" className="w-5 h-4" />
        <ChartPieIcon className="w-5 h-5 text-black" />
      ),
    },
    {
      title: "Devices",
      src: `${garden_id}/device`,
      icon: (
        <img src="/src/assets/Device.png" className="w-5 h-4" color="black" />
      ),
    },
    {
      title: "Calendar",
      src: `${garden_id}/calendar`,
      icon: <FaRegCalendarAlt color="black" className="w-5 h-5" />,
    },
    {
      title: "Plants",
      src: `${garden_id}/plant`,
      icon: <PiPlant color="black" className="w-5 h-5" />,
    },
  ];
  const [selectedMenu, setSelectedMenu] = useState(0);
  return (
    <div
      className={`${
        open ? "w-56" : "w-20"
      } p-5 pt-2 border-r-2 relative duration-300`}
    >
      <div className="sticky top-28">
        <img
          src="/src/assets/control.png"
          className={`absolute cursor-pointer -right-9 top-5 w-7 border-dark-purple
          border-2 rounded-full ${!open && "rotate-180"}`}
          onClick={() => {
            setOpen(!open);
          }}
        />
        <div className="flex-1">
          {/* <div className="flex gap-x-4 items-center">
            <img
              src="/src/assets/Plant.png"
              className={`cursor-default duration-500 w-7 h-8 ${
                open && "rotate-[360deg]"
              } ${!open && "ml-1"}`}
            />
            <h1
              className={`cursor-default text-stone-400 origin-left text-base duration-200 mt-4 ${
                !open && "hidden"
              }`}
            >
              Bảng Điều Khiển
            </h1>
          </div> */}
          <ul className="">
            {Menus.map((Menu, index) => (
              <li
                key={index}
                className={`flex rounded-md p-2 cursor-pointer ${
                  selectedMenu === index ? "bg-gray-100" : ""
                }  hover:bg-gray-100 text-gray-300 text-sm items-center gap-x-3 mt-2`}
                onClick={() => {
                  navigate(`/${Menu.src}`);
                  setSelectedMenu(index); // Update the selected menu
                }}
              >
                {Menu.icon}
                <span
                  className={`${
                    !open && "hidden"
                  } origin-left duration-200 text-black`}
                >
                  {Menu.title}
                </span>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}
