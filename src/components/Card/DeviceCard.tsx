import axios from "axios";
import { NavLink } from "react-router-dom";
import { useParams } from "react-router-dom";
interface DeviceStruct {
  device_id: string;
  device_name: string;
  status: string;
  time_on?: string;
  time_off?: string;
  recommend_time?: number;
  // Add other properties as needed
}

interface DeviceCardProps {
  event: DeviceStruct;
}
export default function DeviceCard({ event }: DeviceCardProps) {
  const { garden_id } = useParams(); // Get the "garden_id" from the URL
  // const [status, setStatus] = useState(event.status);
  const handleButtonClick = async () => {
    event.status = event.status === "ON" ? "OFF" : "ON";
    const respone = await axios({
      method: "POST",
      url: `http://localhost:8080/api/v1/garden/garden_0001/change-device-status`,
      data: {
        garden_id: garden_id,
        id: event.device_id,
        status: event.status,
      },
    });
    console.log(respone);
  };

  return (
    <div className="relative w-auto rounded-lg shadow-md border px-5">
      <div className="h-[35%] flex items-center justify-between pt-3">
        <div className="text-gray-500 font-semibold py-3 text-2xl">
          {event.device_name}
        </div>
        {event.device_name === "Pump1" || event.device_name === "Pump2" ? (
          <div className="sm:w-[100px] h-[50px] relative">
            <NavLink to={`/${garden_id}/device/schedule/${event.device_id}`}>
              <button className="relative w-[50%] h-full text-3xl font-w bg-[#05966A] hover:bg-emerald-700 text-white rounded-full transition-all duration-300 hover:w-[100%] group">
                <span className="absolute left-1/2 top-1/2 transform -translate-x-1/2 text-sm -translate-y-1/2 opacity-0 transition-opacity duration-300 group-hover:opacity-100">
                  Add Schedule
                </span>
                <span className="absolute left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2 opacity-100 transition-opacity duration-300 group-hover:opacity-0">
                  +
                </span>
              </button>
            </NavLink>
          </div>
        ) : null}
      </div>
      {/* {status} */}
      {event.status === "OFF" ? (
        <img
          onClick={handleButtonClick}
          src="/src/assets/offbutton.png"
          className="w-[30%] h-[30%] rounded-lg mb-3"
          alt="avatar"
          style={{ marginLeft: "10px" }}
        />
      ) : (
        <img
          src="/src/assets/onbutton.png"
          onClick={handleButtonClick}
          className="w-[30%] h-[30%] rounded-lg mb-3"
          alt="avatar"
          style={{ marginLeft: "10px" }}
        />
      )}
      <div className="flex h-[25%] items-center justify-center relative max-lg:hidden">
        {event.device_name === "Pump1" || event.device_name === "Pump2" ? (
          event.status === "OFF" ? (
            <img
              src="/src/assets/waterpump.png"
              className="w-[50%] h-[50%] rounded-lg mb-3"
              alt="avatar"
            />
          ) : (
            <img
              src="/src/assets/waterpumpon.png"
              className="w-[50%] h-[50%] rounded-lg mb-3"
              alt="avatar"
            />
          )
        ) : event.device_name === "Light" ? (
          event.status === "OFF" ? (
            <img
              src="/src/assets/lightbulb.png"
              className="w-[50%] h-[50%] rounded-lg mb-3"
              alt="avatar"
            />
          ) : (
            <img
              src="/src/assets/lightbulbon.png"
              className="w-[50%] h-[50%] rounded-lg mb-3"
              alt="avatar"
            />
          )
        ) : event.device_name === "Fan" ? (
          event.status === "OFF" ? (
            <img
              src="/src/assets/fan.png"
              className="w-[50%] h-[50%] rounded-lg mb-3"
              alt="avatar"
            />
          ) : (
            <img
              src="/src/assets/fanon.png"
              className="w-[50%] h-[50%] rounded-lg mb-3"
              alt="avatar"
            />
          )
        ) : null}
      </div>
      <div className="h-[15%]"></div>
    </div>
  );
}
